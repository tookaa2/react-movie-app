import React, { Component } from 'react';
import { Form, Button, Input, Icon, message } from 'antd';
import { withRouter } from 'react-router-dom';
const KEY_USER_DATA = 'user_data';
class Login extends Component {
  state = {
    email: '',
    password: '',
    errorVisible: false
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push('/movies');
  };
  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };
  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }
  onSubmitForm = event => {
    event.preventDefault();
    console.log('askdjaklsdjkasjdkl');
    const isEmailValid = this.validateEmail(this.state.email);
    if (isEmailValid) {
      if (this.validatePassword(this.state.password)) {
        localStorage.setItem(
          KEY_USER_DATA,
          JSON.stringify({
            isLoggedIn: true
          })
        );

        this.navigateToMainPage();
      } else {
        console.log('pass ผิด');
      }
    } else {
      this.setState({ errorVisible: true });
      message.error('Email or Password Invalid');
    }
  };
  render() {
    return (
      <div>
        <Form onSubmit={this.onSubmitForm}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" />}
              type="user"
              placeholder="Email"
              onChange={this.onEmailChange}
            />
          </Form.Item>

          <Form.Item>
            <Input
              prefix={<Icon type="lock" />}
              type="password"
              placeholder="Password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>

          <Form.Item>
            <Button htmlType="submit">Submit</Button>
          </Form.Item>
        </Form>
        <img
          style={{ display: this.state.errorVisible ? 'flex' : 'none' }}
          src="https://media1.giphy.com/media/qmfpjpAT2fJRK/giphy.gif?cid=3640f6095c4e89f4716e46386f32c2b5"
        />
        <h1 style={{ display: this.state.errorVisible ? 'flex' : 'none' }}>
          Wrong format :( !!!!!!!!
        </h1>
      </div>
    );
  }
}

export default withRouter(Login);
