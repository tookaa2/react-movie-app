import { List } from 'antd';
import React, { Component } from 'react';
import ItemFavorite from './item';
class ListFavorite extends Component {
  state = {
    items: []
  };
  componentDidMount() {
    const jsonStr = localStorage.getItem('list-fav');
    const items = JSON.parse(jsonStr) || [];
    this.setState({ items });
  }
  render() {
    return (
      <div>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.state.items}
          renderItem={item => (
            <List.Item>
              <ItemFavorite
                item={item}
                onItemMvieclick={this.props.onItemMvieclick}
              />
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default ListFavorite;
