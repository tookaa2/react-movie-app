import React, { Component } from 'react';
import { Spin, Layout, Button, Menu, Modal, message } from 'antd';
import RouteMenu from '../Containers/RouteMenu';
import { connect } from 'react-redux';

const menus = ['movies', 'favorite', 'profile'];
const { Header, Content, Footer } = Layout;
const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    itemMovieClick: state.itemMovieDetail
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: 'dismiss_dialog'
      }),
    onItemMovieClick: item =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  };
};
class Main extends Component {
  state = {
    items: [],

    itemMovie: null,
    pathName: menus[0],
    favItems: []
  };

  // onClickBuyTicker = () => {};

  modalOk = () => {
    this.props.onDismissDialog();
  };
  modalCancel = () => {
    this.props.onDismissDialog();
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem('list-fav');
    const items = JSON.parse(jsonStr) || [];
    if (items) {
      this.setState({ favItems: items });
    }
    const { pathname } = this.props.location;
    var pathName = menus[0];
    if (pathname != '/') {
      pathName = pathname.replace('/', '');
      if (!menus.includes(pathName)) pathName = menus[0];
    }
    this.setState({ pathName });
    fetch('https://workshopup.herokuapp.com/movie')
      .then(response => response.json())
      .then(items => this.setState({ items: items.results }));
  }

  onMenuClick = event => {
    var path = '/';
    if (event.key != 'movies') {
      path = `/${event.key}`;
    }
    this.props.history.replace(path);
  };
  onClickFavorite = () => {
    const item = this.props.itemMovieClick;
    const favoriteList = this.state.favItems;
    const alreadyHave = favoriteList.find(thisItem => {
      return thisItem.title === item.title;
    });

    if (alreadyHave) {
      message.error('Already Favorited');
    } else {
      favoriteList.push(item);
      localStorage.setItem('list-fav', JSON.stringify(favoriteList));
      message.success('Favorite saved');
      this.modalCancel();
    }
  };
  render() {
    const item = this.props.itemMovieClick;
    return (
      <div>
        {this.state.items.length > 0 ? (
          <div style={{ height: '100vh' }}>
            <Layout>
              <Header
                style={{
                  padding: '0px',
                  position: 'fixed',
                  zIndex: 1,
                  width: '100%'
                }}
              >
                <Menu
                  theme="light"
                  mode="horizontal"
                  defaultSelectedKeys={[this.state.pathName]}
                  style={{ lineHeight: '64px', width: '100%' }}
                  onClick={e => {
                    this.onMenuClick(e);
                  }}
                >
                  <Menu.Item key={menus[0]}>Home</Menu.Item>
                  <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                  <Menu.Item key={menus[2]}>Profile</Menu.Item>
                </Menu>
              </Header>
              <Content
                style={{
                  padding: '16px',
                  marginTop: 64,
                  minHeight: '600px',
                  justifyContent: 'center',
                  alignItems: 'center',
                  display: 'flex'
                }}
              >
                <RouteMenu items={this.state.items} />
              </Content>
              <Footer style={{ textAlign: 'center', background: 'white' }}>
                Movie Application workshop @ CAMT
              </Footer>
            </Layout>
          </div>
        ) : (
          <Spin size="large" />
        )}
        {item !== null ? (
          <Modal
            title={item.title}
            visible={this.props.isShowDialog}
            onOk={this.modalOk}
            onCancel={this.modalCancel}
            footer={[
              <Button
                key="submit"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
              />,
              <Button
                key="submit"
                type="primary"
                icon="shopping-cart"
                size="large"
                shape="circle"
                onClick={this.onClickBuyTicker}
              />
            ]}
          >
            <img src={item.image_url} style={{ width: '100%' }} />
            <br />
            <br />
            <span>{item.overview}</span>
          </Modal>
        ) : null}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
